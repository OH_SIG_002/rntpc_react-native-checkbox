# Changelog

## v0.5.17-rc.1

* chore: remove android and ios code [#2](https://gitee.com/openharmony-sig/rntpc_react-native-checkbox/pulls/2)
* feat: add OpenHarmony support [#3](https://gitee.com/openharmony-sig/rntpc_react-native-checkbox/pulls/3)
* pre-release: @react-native-ohos/checkbox@0.5.17-rc.1 [#6](https://gitee.com/openharmony-sig/rntpc_react-native-checkbox/pulls/6)