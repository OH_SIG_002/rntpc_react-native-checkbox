# @react-native-ohos/checkbox

This project is based on [@react-native-community/checkbox](https://github.com/react-native-checkbox/react-native-checkbox)

## Documentation

- [中文](https://gitee.com/react-native-oh-library/usage-docs/blob/master/zh-cn/react-native-community-checkbox.md)

- [English](https://gitee.com/react-native-oh-library/usage-docs/blob/master/en/react-native-community-checkbox.md)

## License

This library is licensed under [The MIT License (MIT)](https://gitee.com/openharmony-sig/rntpc_react-native-checkbox/blob/master/LICENSE)